# hellohi

Visualizing Hello Internet's community activity

This project is intended to create a visualization tool/webapp for monitoring the Hello Internet subreddit, and eventually other platforms. The goal is to develop some visualization techniques that are more broadly applicable, with a constrained context to help focus development on the visuals and methods. This project is purely for fun and education.